# README #

* [South African Tuberculosis Vaccine Initiative](http://www.satvi.uct.ac.za/)

### Simon’s Selection of Six Small Signatures for SHIP (SixS) ###

A head-to-head assessment of 6 pasimonious transcriptomics signatures in tuberculosis prognosis, diagnosis & treatment response. The SIXS project is a companion project of the Correlate of Risk Targeted Intervention Study (CORTIS) of Isoniazid and Rifapentine (3HP) therapy to prevent pulmonary Tuberculosis in high-risk individuals identified by the transcriptomic signature. In parallel, the SIXS project will also in the CORTIS-HR study look at whether the prognostic correlate of risk status differentiates HIV infected persons with cumulative prevalent or incident TB disease from those without TB disease.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Schematics of the QC workflow ###
![](figures/SixS_QC_v5.png)

### Who do I talk to? ###

* stanley.kimbung@uct.ac.za
* simon.mendelsohn@uct.ac.za

Version 5.00